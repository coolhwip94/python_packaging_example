# Python Packaging
> This repo will host an example of creating a python package so that it can be referenced later

## Overview
---
> The package layout should consist of the following structure
```
python_packaging_example/
├── LICENSE
├── pyproject.toml
├── readme.md
├── setup.cfg
├── src/
│   ├── example_package/
│   ├── __init__.py
│   └── example.py
└── tests/
```

## Directories and Files
---

- `tests/` - empty placeholder for now
- `pyproject.toml` = Defines requirements for project build
    - `build-system.requires` gives a list of packages that are needed to build your package. Listing something here will only make it available during the build, not after it is installed.
    - `build-system.build-backend` is the name of Python object that will be used to perform the build

- `setup.cfg` - It tells setuptools about your package (such as the name and version) as well as which code files to include. (metadata)
    - `setup.py` - can also be used for dynamic metada

- `LICENSE` - This tells users who install your package the terms under which they can use your package.

<br>

## Build
---
- To build the project first install/update the build package
    ```
    python3 -m pip install --upgrade build
    ```
- Run build command from same directory as `pyproject.toml`
    ```
    python3 -m build
    ```
- Running the build command will create a dist folder with the package inside
    ```
    dist
    ├── example-pkg-ltruong-0.0.1.tar.gz
    └── example_pkg_ltruong-0.0.1-py3-none-any.whl
    ```



## Upload
---
> This will cover uploading to https://test.pypi.org/, if following along, you will need to sign up for an account and generate an api token
- Upgrade twine
    ```
    python3 -m pip install --upgrade twine
    ```
- Run twine to upload files in `dist` directory.
    > This will prompt for username/password. (username : `__token__` / Password : `<your_api_token`)
    ```
    python3 -m twine upload --repository testpypi dist/*
    ```

- Example of uploaded package : https://test.pypi.org/project/example-pkg-ltruong/0.0.1/


## Install

- Install the uploaded package
    ```
    pip3 install -i https://test.pypi.org/simple/ example-pkg-ltruong==0.0.1
    ```

## Usage
- You can now utilize the installed package

    ```python
    from example_package import example

    example.add_one(2)
    example.print_hello()
    ```

## Resources
---
- https://packaging.python.org/tutorials/packaging-projects/